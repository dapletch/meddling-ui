let options = {
    dataType: 'json',
    // disabling certain attributes for we'll manage that on our end while building the form
    // https://formbuilder.online/docs/formBuilder/options/disabledAttrs/
    disabledAttrs: [
        'access',
        'className',
        'inline',
        'maxlength',
        'rows',
        'subtype',
        'other',
        'toggle'
    ],
    // disabling certain form elements that can be used for the purposes of the formBuilder
    // https://formbuilder.online/docs/formBuilder/options/disableFields/
    disableFields: [
        'autocomplete',
        'button',
        'file'
    ],
    // onSave option enables custom functionality for the SAVE button rendered by formBuilder
    // https://formbuilder.online/docs/formBuilder/options/onSave/
    onSave: function (evt, formData) {
        // TODO make URL relative
        $.post("http://localhost:8080/api/formBuilder",
            {
                'setting': setting,
                'formData': formData
            },
            function (data, status) {
                if (status === 'success') {
                    // display the modal
                    $('#formStatusModalText').text(data.message);
                    $('#formStatusModal').modal('show');
                } else {
                    console.error(`An error occurred: ${status}`);
                }
            }
        );
    },
};

// formData and type are attributes responsible for setting the default form values
// https://formbuilder.online/docs/formBuilder/options/formData/
let formConfig = [
    // formBuilder tab 1
    {
        'fb-editor-1': FormBuilderDefaults.DEFAULT_FORM_INPUTS["GENERAL"]
    },
    // formBuilder tab 2
    {
        'fb-editor-2': FormBuilderDefaults.DEFAULT_FORM_INPUTS["SUBJECTS"]
    },
    // formBuilder tab 3
    {
        'fb-editor-3': FormBuilderDefaults.DEFAULT_FORM_INPUTS["ADDRESSES"]
    },
    // formBuilder tab 4
    {
        'fb-editor-4': FormBuilderDefaults.DEFAULT_FORM_INPUTS["VEHICLES"]
    }
];

for (let i = 0; i < formConfig.length; i++) {
    jQuery(
        function ($) {
            options.formData = JSON.stringify(Object.values(formConfig[i])[0]);
            $(document.getElementById(Object.keys(formConfig[i])[0])).formBuilder(options);
        }
    );
}